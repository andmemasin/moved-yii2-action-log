<?php


$config = [
    'id' => 'collector-test-app',
    'basePath' => dirname(__DIR__). "/../src/",
    'bootstrap' => ['log'],
    'modules' => [
        'actionlog' => [
            'class' => 'andmemasin\actionlog\Module',
            'permissionMap'=>[
                'admin'=>'siteAdmin',
                'delete'=>'siteAdmin'
            ],
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'aliases' =>[
        '@vendor' => '@app/../vendor',
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../../src/web/assets',
        ],
        'db' =>   require(__DIR__ . '/db.php'),
        'i18n' => [
            'translations' => [
                'actionlog*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
            'url' => "http://localhost"
        ],
        'user' => [
            'identityClass' => \yii\web\User::class,
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
        'log' => [
            'traceLevel' => 0,
            'flushInterval' => 1,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile'=> __DIR__.'/../_output/logs/app.log',
                    'levels' => ['error', 'warning', 'info', 'trace'],
                    'exportInterval' => 1,
                    // do not log context
                    'logVars' => [],
                ],

            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],

];



return $config;
