<?php

namespace andmemasin\actionlog\tests\functional;


use andmemasin\actionlog\controllers\ActionLogController;
use andmemasin\actionlog\models\ActionLog;
use Codeception\Stub;
use Codeception\Test\Unit;
use yii\base\Action;
use yii\web\IdentityInterface;
use yii\web\Request;
use andmemasin\actionlog\Helper\Setting;

/**
 * @package andmemasin\actionlog\tests\functional
 * @author Tõnis Ormisson <tonis@andmemasin.eu>
 */
class ActionLogTest extends Unit
{
    /** @var Setting */
    public $model;

    protected function setUp() :void
    {
        // Setting is a model from my-abstracts tha implements actionlog behabiours
        $this->model = new Setting([
            'key' => "some-key",
            'value' => "some-value",
        ]);

        $request = $this->mockRequest();
        $module = \Yii::$app->getModule('actionlog');
        \Yii::$app->set('request', $request);
        /** @var IdentityInterface $user */
        $user = $this->createMock(IdentityInterface::class);
        \Yii::$app->user->login($user);

        \Yii::$app->controller = new ActionLogController('fake', $module);

        parent::setUp();
    }

    public function testAfterInsert() {
        $this->model->validate();
        $this->assertTrue($this->model->save());
        $log = ActionLog::find()->one();
        if (empty($log)) {
            throw new \Exception('failed to save model: ' . serialize($this->model->errors));
        }
        $expectedMessage = serialize("andmemasin\actionlog\behaviors\ActionLogBehavior::afterInsert");
        $this->assertEquals($expectedMessage, $log->message);
        $this->assertEquals(Setting::class, $log->class);
    }

    public function testAfterUpdate() {
        $this->model->save();
        $this->model->value = "some-new-value";
        $this->assertTrue($this->model->save());
        // find the 2nd log (1st is for insert)
        $log = ActionLog::findOne(2);
        $this->assertInstanceOf(ActionLog::class, $log);
        $expectedMessage = serialize("andmemasin\actionlog\behaviors\ActionLogBehavior::afterUpdate");
        $this->assertEquals($expectedMessage, $log->message);
        $this->assertEquals(1, $log->model_id);
        $this->assertEquals(Setting::class, $log->class);
    }

    public function testAfterDelete() {
        $this->model->save();
        $this->model->delete();

        // find the 2nd log (1st is for insert & update)
        $log = ActionLog::findOne(3);
        $this->assertInstanceOf(ActionLog::class, $log);
        $expectedMessage = serialize("andmemasin\actionlog\behaviors\ActionLogBehavior::afterDelete");
        $this->assertEquals($expectedMessage, $log->message);
        $this->assertEquals(1, $log->model_id);
        $this->assertEquals(Setting::class, $log->class);
    }

    public function testAdd()
    {
        $result = ActionLog::add('debug', 'fake', 1, $this->model);
        $this->assertTrue($result);
    }

    public function testAddFails()
    {
        $result = ActionLog::add('debug', 'fake', 1, $this->model);
        $this->assertTrue($result);
    }

    private function  mockRequest(){
        // mock a request
        $_SERVER['REQUEST_URI'] = 'http://localhost';
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        \Yii::$app->requestedAction = new Action('fake', $this->model);
        \Yii::$app->setHomeUrl('http://localhost');
        return Stub::make(Request::class, [
            'getUserIP' =>'127.0.0.1',
            'enableCookieValidation' => false,
            'getUserAgent' => 'Dummy User Agent',
        ]);
    }

}
