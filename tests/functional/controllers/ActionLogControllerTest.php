<?php
namespace andmemasin\actionlog\functional\controllers;

use andmemasin\actionlog\controllers\ActionLogController;
use andmemasin\actionlog\models\ActionLog;
use andmemasin\collector\controllers\CollectorController;
use andmemasin\collector\models\Collector;
use andmemasin\collector\models\CollectorType;
use andmemasin\helpers\ViewTag;
use andmemasin\myabstract\MyActiveRecord;
use Codeception\Stub;
use Codeception\Test\Unit;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;

class ActionLogControllerTest extends Unit
{
    /** @var CollectorController */
    public $model;


    public function setUp() : void
    {
        $config = require( __DIR__ . "/../../_config/test.php");
        $config['basePath'] = __DIR__ . "/../../../src";
        $config['aliases']['@vendor'] = '@app/../vendor';
        $this->model = new ActionLogController('action-log', new \yii\web\Application($config));
        \Yii::$app->controller = $this->model;
        \Yii::$app->controller->action = new Action('fake', $this->model);
        parent::setUp();

    }


    public function testActionIndex()
    {
        $result = $this->model->actionIndex();
        $tag = (string) new ViewTag('actionLogIndex');
        $this->assertStringContainsString($tag, $result);
    }

    public function testActionView()
    {
        $this->mockRequest([]);
        ActionLog::add('debug', 'fake');
        $result = $this->model->actionView(1);
        $tag = (string) new ViewTag('actionLogView');
        $this->assertStringContainsString($tag, $result);
    }

    public function testActionViewFails()
    {
        $this->expectException(NotFoundHttpException::class);
        $this->model->actionView(9999);
    }

    private function  mockRequest($data){
        // mock a request
        $_SERVER['REQUEST_URI'] = 'http://localhost';
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        \Yii::$app->requestedAction = new Action('fake', $this->model);
        \Yii::$app->setHomeUrl('http://localhost');
        return Stub::make(Request::class, [
            'getUserIP' =>'127.0.0.1',
            'enableCookieValidation' => false,
            'getUserAgent' => 'Dummy User Agent',
            'getBodyParams' => [
                'Collector' => $data
            ],
        ]);
    }

    private function goodConf()
    {
        return [
            'type' => CollectorType::TYPE_LIMESURVEY,
            'label' => 'Test label',
            'interview_url' => 'https://example.com/',
            'credentials' => '{"username":"","password":""}',
            'api_url' => 'https://example.com/',
        ];

    }


}
