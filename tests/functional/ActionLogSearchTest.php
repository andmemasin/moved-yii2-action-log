<?php

namespace andmemasin\actionlog\tests\functional;


use andmemasin\actionlog\controllers\ActionLogController;
use andmemasin\actionlog\models\ActionLog;
use andmemasin\actionlog\models\ActionLogSearch;
use andmemasin\myabstract\Setting;
use Codeception\Stub;
use Codeception\Test\Unit;
use yii\base\Action;
use yii\data\ActiveDataProvider;
use andmemasin\myabstract\test\ModelTestTrait;
use yii\web\IdentityInterface;
use yii\web\Request;
use yii\web\User;

/**
 * @package andmemasin\actionlog\tests\functional
 * @author Tõnis Ormisson <tonis@andmemasin.eu>
 */
class ActionLogSearchTest extends Unit
{
    /** @var ActionLogSearch */
    private $model;


    protected function _before()
    {
        $this->model = Stub::make(ActionLogSearch::class);
        $module = \Yii::$app->getModule('actionlog');
        \Yii::$app->controller = new ActionLogController('fake', $module);
    }

    protected function _after()
    {
    }



    public function testSearch() {
        $result = $this->model->search([]);
        $this->assertInstanceOf(ActiveDataProvider::class, $result);
    }

    public function testSearchWhileValidationFails() {
        $this->model->user_id = "not-an-integer-will-fail-validation";
        $result = $this->model->search([]);
        $this->assertInstanceOf(ActiveDataProvider::class, $result);
    }

}
