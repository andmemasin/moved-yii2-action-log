<?php


namespace andmemasin\actionlog\Helper;


class Setting extends \andmemasin\myabstract\Setting
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge([
            'actionlog' => [
                'class' => 'andmemasin\actionlog\behaviors\ActionLogBehavior',
            ],
        ], parent::behaviors());
    }

}
