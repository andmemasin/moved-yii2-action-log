
--
-- Table structure for table `actionlog`
--

CREATE TABLE `actionlog` (
  `actionlog_id` int(11) NOT NULL COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT 'User',
  `module` varchar(255) DEFAULT NULL COMMENT 'Module',
  `controller` varchar(512) NOT NULL COMMENT 'Controller',
  `action` varchar(255) NOT NULL COMMENT 'Action',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `message` text DEFAULT NULL COMMENT 'Message',
  `class` varchar(255) DEFAULT NULL COMMENT 'Class',
  `model_id` int(11) DEFAULT NULL COMMENT 'Model id',
  `time` datetime NOT NULL COMMENT 'Time',
  `ip` varchar(128) NOT NULL COMMENT 'IP address',
  `user_created` int(11) NOT NULL DEFAULT 1 COMMENT 'Created by',
  `user_updated` int(11) NOT NULL DEFAULT 1 COMMENT 'Updated by',
  `user_closed` int(11) DEFAULT NULL COMMENT 'Closed by',
  `time_created` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Created at',
  `time_updated` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Updated at',
  `time_closed` datetime NOT NULL DEFAULT '3000-12-31 00:00:00' COMMENT 'Closed at'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actionlog`
--
ALTER TABLE `actionlog`
  ADD PRIMARY KEY (`actionlog_id`),
  ADD KEY `ix_user_id` (`user_id`),
  ADD KEY `ix_controller` (`controller`),
  ADD KEY `ix_module` (`module`),
  ADD KEY `ix_status` (`status`),
  ADD KEY `ix_model_id` (`model_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actionlog`
--
ALTER TABLE `actionlog`
  MODIFY `actionlog_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';
COMMIT;


CREATE TABLE `setting` (
  `setting_id` int(11) NOT NULL COMMENT 'ID',
  `key` varchar(128) DEFAULT NULL,
  `value` varchar(255) NOT NULL COMMENT 'Action',
  `user_created` int(11) NOT NULL DEFAULT 1 COMMENT 'Created by',
  `user_updated` int(11) NOT NULL DEFAULT 1 COMMENT 'Updated by',
  `user_closed` int(11) DEFAULT NULL COMMENT 'Closed by',
  `time_created` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Created at',
  `time_updated` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Updated at',
  `time_closed` datetime NOT NULL DEFAULT '3000-12-31 00:00:00' COMMENT 'Closed at'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`);

ALTER TABLE `setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';
COMMIT;



CREATE TABLE `closing` (
  `table_name` varchar(128) NOT NULL COMMENT 'referred entity table name',
  `last_closing_time` datetime NOT NULL COMMENT 'Last closure time in that entity',
  `user_created` int(11) NOT NULL DEFAULT 1 COMMENT 'Created by',
  `user_updated` int(11) NOT NULL DEFAULT 1 COMMENT 'Updated by',
  `user_closed` int(11) DEFAULT NULL COMMENT 'Closed by',
  `time_created` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Created at',
  `time_updated` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Updated at',
  `time_closed` datetime NOT NULL DEFAULT '3000-12-31 00:00:00' COMMENT 'Closed at'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `closing`
  ADD PRIMARY KEY (`table_name`);
COMMIT;

CREATE TABLE `user` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `username` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';
COMMIT;
