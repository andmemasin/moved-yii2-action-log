<?php

namespace andmemasin\actionlog\tests\unit;


use andmemasin\actionlog\controllers\ActionLogController;
use andmemasin\myabstract\test\ModelTestTrait;
use Codeception\Stub;
use Codeception\Test\Unit;
use andmemasin\actionlog\models\ActionLog;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\web\Request;

class ActionLogControllerTest extends Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var ActionLogController */
    private $model;


    protected function _before()
    {
        $request = $this->mockRequest();
        $module = \Yii::$app->getModule('actionlog');
        $this->model = new ActionLogController('fake', $module);
        \Yii::$app->set('request', $request);
    }

    public function testBehaviors() {
        $this->assertArrayHasKey('access', $this->model->behaviors());

    }




    private function  mockRequest(){
        // mock a request
        $_SERVER['REQUEST_URI'] = 'http://localhost';
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        \Yii::$app->requestedAction = new Action('fake', $this->model);
        \Yii::$app->setHomeUrl('http://localhost');
        return Stub::make(Request::class, [
            'getUserIP' =>'127.0.0.1',
            'enableCookieValidation' => false,
            'getUserAgent' => 'Dummy User Agent',
        ]);
    }

}