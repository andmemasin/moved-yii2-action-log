<?php

namespace andmemasin\actionlog\tests\unit;


use andmemasin\actionlog\controllers\ActionLogController;
use andmemasin\actionlog\models\ActionLogSearch;
use andmemasin\myabstract\test\ModelTestTrait;
use Codeception\Stub;
use Codeception\Test\Unit;
use andmemasin\actionlog\models\ActionLog;
use yii\base\Action;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Request;

class ActionLogSearchTest extends Unit
{
    use ModelTestTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var ActionLogSearch */
    private $model;


    protected function _before()
    {
        $this->model = Stub::make(ActionLogSearch::class);
        $module = \Yii::$app->getModule('actionlog');
        \Yii::$app->controller = new ActionLogController('fake', $module);
    }

    protected function _after()
    {
    }

    public function testScenarios() {
        $this->assertIsArray( $this->model->scenarios());
    }



}
