<?php

namespace andmemasin\actionlog\tests\unit;


use andmemasin\actionlog\controllers\ActionLogController;
use andmemasin\collector\models\CollectorType;
use andmemasin\myabstract\Setting;
use andmemasin\myabstract\test\ModelTestTrait;
use Codeception\Stub;
use Codeception\Test\Unit;
use andmemasin\actionlog\models\ActionLog;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\web\Request;

class ActionLogTest extends Unit
{
    use ModelTestTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var ActionLog */
    private $model;


    protected function setUp() : void
    {
        $this->model = $this->baseObject();
        parent::setUp();
    }

    public function testGetUserId() {
        $result = $this->model::getUserID();
        $this->assertEquals(0, $result);
    }

    public function testGetUserIdInConsole() {
        $app = $this->mockApplication();
        \Yii::$app = $app;
        $result = $this->model::getUserID();
        $this->assertEquals(0, $result);
    }

    protected function mockApplication($config = [], $appClass = '\yii\console\Application')
    {
        return new $appClass(ArrayHelper::merge([
            'id' => 'testapp',
            'basePath' => __DIR__,
            'vendorPath' => \Yii::$app->vendorPath,
        ], $config));
    }

    /**
     * Returns a good working LimeSurvey collector
     * @return ActionLog
     */
    public function baseObject(){
        /** @var ActionLog $model */
        $model = Stub::make(ActionLog::class, [
            'attributes' => array_keys($this->baseModelAttributes()),
        ]);
        $model->setAttributes($this->baseModelAttributes());
        $model->module = \Yii::$app->getModule('collector');

        return $model;
    }


    /**
     * @return array
     */
    protected function baseModelAttributes() {
        return [
            'actionlog_id' => 1,
            'user_id' => 1,
            'model_id' => 1,
            'class' => 'MyClass',
            'module' => 'MyModule',
            'controller' => 'myController',
            'action' => 'myAction',
            'status' => 'statusok',
            'message' => 'some-message',
            'time' => '2020-12-12 10:10:10',
            'ip' => '1.1.1.1',
        ];
    }

}
