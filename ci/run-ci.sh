#!/bin/bash

echo "writing key $1"
# NB the key must be stored in bitbucket as base64, and needs to be decoded here
echo $1 | base64 --decode > /root/.ssh/id_rsa
cat /root/.ssh/id_rsa

echo "writing key permissions"
chmod 600 /root/.ssh/id_rsa

echo "checking key"
ls -lsta /root/.ssh
cat /root/.ssh/id_rsa

echo "running composer create"
composer create-project
composer  install
