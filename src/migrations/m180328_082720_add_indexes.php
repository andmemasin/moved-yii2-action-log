<?php

use yii\db\Migration;

/**
 * Class m180328_082720_add_indexes
 */
class m180328_082720_add_indexes extends Migration
{
    private $columns = [];
    private $tableName;
    private $indexPrefix = 'ix_';


    private function setUp() {
        $this->tableName = 'actionlog';
        $this->columns =[
            'user_id', 'controller', 'module', 'status',
             'model_id'
        ];

    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->setUp();
        foreach ($this->columns as $column) {
            $this->createIndex($this->indexPrefix.$column,$this->tableName, $column);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->setUp();
        foreach ($this->columns as $column) {
            $this->dropIndex($this->indexPrefix.$column,$this->tableName);
        }
    }

}
