<?php

use yii\db\Migration;

class m170313_164635_initial_populate extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%actionlog}}', [
            'actionlog_id' => $this->primaryKey()->comment('ID'),
            'user_id' => $this->integer()->notNull()->comment('User'),
            'module' => $this->string(255)->null()->comment('Module'),
            'controller' => $this->string(512)->notNull()->comment('Controller'),
            'action' => $this->string(255)->notNull()->comment('Action'),
            'status' => $this->string(32)->null()->comment('Status'),
            'message' => $this->text()->null()->comment('Message'),
            'class' => $this->string(255)->null()->comment('Class'),
            'model_id' => $this->integer()->null()->comment('Model id'),
            'time' => $this->dateTime(6)->notNull()->comment('Time'),
            'ip' => $this->string(128)->notNull()->comment('IP address'),
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%actionlog}}');
    }
}
