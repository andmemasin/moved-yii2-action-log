<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model andmemasin\actionlog\models\ActionLog */

$this->title = $model->actionlog_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Action Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// DO NOT REMOVE This is for automated testing to validate we see that page
echo new \andmemasin\helpers\ViewTag('actionLogView');

?>
<div class="action-log-view">


    <div class="panel panel-default">
        <div class="panel-body">

            <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'actionlog_id',
                'user_id',
                'module',
                'controller',
                'action',
                'status',
                'message:ntext',
                'time',
                'ip',
                ],
            ]) ?>

        </div>
    </div>
</div>
