<?php

use andmemasin\actionlog\Module;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel andmemasin\actionlog\models\ActionLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('actionlog', 'Action Logs');
$this->params['breadcrumbs'][] = $this->title;

// DO NOT REMOVE This is for automated testing to validate we see that page
echo new \andmemasin\helpers\ViewTag('actionLogIndex');

?>
<div class="action-log-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-striped table-bordered table-responsive'],
                'columns' => [

                    'time',
                    'ip',
                    'user.username',
                    'model_id',
                    'module',
                    'class',
                    'controller',
                    'action',
                ],
            ]); ?>
            
        </div>
    </div>

</div>
