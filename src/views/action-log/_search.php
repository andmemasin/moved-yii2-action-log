<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model \andmemasin\actionlog\models\ActionLogSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(<<<JS
$("#actionlog-search-form").hide();

$('.toggle-actionlog-search').click(function() {
    $("#actionlog-search-form").toggle(500);
});
JS
    , View::POS_READY);

?>


<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="toggle-actionlog-search link"><i class="fa fa-search toggle-actionlog-search link"></i> <?= Yii::t('app','Search')?></span>
    </div>
    <div class="panel-body" id="actionlog-search-form">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>


        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'module') ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'controller') ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'action') ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'class') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'username') ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'model_id') ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'status') ?>
            </div>

            <div class="col-lg-4">
                <?= $form->field($model, 'ip') ?>
            </div>

        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            <?= Html::resetButton(Yii::t('app', 'Close'), ['class' => 'btn btn-default toggle-actionlog-search pull-right']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
