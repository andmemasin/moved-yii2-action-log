<?php

namespace andmemasin\actionlog\models;

use andmemasin\actionlog\factories\ActionLogFactory;
use andmemasin\actionlog\interfaces\Loggable;
use andmemasin\myabstract\User;
use Yii;
use yii\db\ActiveRecord;
use andmemasin\helpers\DateHelper;
use andmemasin\actionlog\Module;

/**
 * This is the model class for table "actionlog".
 *
 * @property integer $actionlog_id
 * @property integer $user_id
 * @property integer $model_id
 * @property string $class Model className
 * @property string $module
 * @property string $controller
 * @property string $action
 * @property string $status
 * @property string $message
 * @property string $time
 * @property string $ip
 * @property User $user
 *
 * @property ActionLog[] $userRecords all records of current user
 * @property ActionLog[] $followingRecords
 * @property ActionLog $nextLogin
 * @property ActionLog[] $recordsTilNextLogin
 */
class ActionLog extends \andmemasin\myabstract\ActiveRecord implements Loggable
{
    const LOG_STATUS_SUCCESS = 'success';
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%actionlog}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'controller', 'action', 'time', 'ip'], 'required'],
            [['user_id','model_id'], 'integer'],
            [['message'], 'string'],
            [['time','class'], 'safe'],
            [['module', 'action'], 'string', 'max' => 255],
            [['controller'], 'string', 'max' => 512],
            [['status'], 'string', 'max' => 32],
            [['ip'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'actionlog_id' => Module::t('actionlog', 'ID'),
            'user_id' => Module::t('actionlog', 'User ID'),
            'module' => Module::t('actionlog', 'Module'),
            'controller' => Module::t('actionlog', 'Controller'),
            'action' => Module::t('actionlog', 'Action'),
            'status' => Module::t('actionlog', 'Status'),
            'message' => Module::t('actionlog', 'Message'),
            'time' => Module::t('actionlog', 'Time'),
            'ip' => Module::t('actionlog', 'Ip'),
        ];
    }

    /**
     * Adds a message to ActionLog model
     *
     * @param string $status The log status information
     * @param mixed $message The log message
     * @param int $user_id The user id
     * @param ActiveRecord $model
     * @return bool
     */
    public static function add($status = 'debug', $message = null, $user_id = 0, $model = null)
    {
        /** @var ActionLog $log */
        $log = (new ActionLogFactory)->makeBaseLog();

        if (empty($log)){
            return false;
        }

        $log->user_id = ((int)$user_id !== 0) ? (int)$user_id : (int)$log->getUserID();

        $log->message = ($message !== null) ? serialize($message) : null;
        $modelInfo = '';

        if (is_object($model)) {
            if (isset($model->primaryKey)) {
                $log->model_id = $model->primaryKey;
            }

            $log->class = get_class($model);
            $modelInfo = $log->model_id;
        }

        if(is_string($model)){
            $log->class = $model;
        }

        Yii::$status('UserId '.$log->user_id.': '.$log->module.'/'.$log->controller.'/'.$log->action.' '.$modelInfo, __METHOD__);

        if(!$log->save()){
            Yii::error('Error saving action log: '.serialize($log->errors),__METHOD__);
        };
        return true;
    }

    /**
     * Get the current user ID
     *
     * @return int The user ID
     */
    public static function getUserID()
    {
        // console
        if (Yii::$app instanceof Yii\console\Application){
            return 0;
        }
        // Web
        $user = Yii::$app->getUser();
        return $user && !$user->getIsGuest() ? $user->getId() : 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\NotSupportedException
     */
    public function getUserRecords()
    {
        $query = self::find()->andWhere(['user_id' => $this->user_id]);
        $query->orderBy([$this->primaryKeySingle() => SORT_DESC]);
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\NotSupportedException
     */
    public function getFollowingRecords()
    {
        $query = $this->getUserRecords();
        $query->andWhere(['>', $this->primaryKeySingle(), $this->primaryKey]);
        return $query;
    }

    /**
     * @return array|ActiveRecord|null
     */
    public function getNextLogin()
    {
        $query = $this->getFollowingRecords();
        $query->andWhere([
            'module' => 'user',
            'controller' => 'security',
            'action' => 'login'
        ])
        ->limit(1);

        return $query->one();
    }

    /** {@inheritdoc} */
    public function getUserColumn()
    {
        return 'user_id';
    }

    /** {@inheritdoc} */
    public function getLastActions()
    {
        /** @var Module $module */
        $module = \Yii::$app->getModule('actionlog');
        $query = self::find();
        $query->orderBy(['actionlog_id' => SORT_DESC]);
        return $query;
    }

    /** {@inheritdoc} */
    public function getLastLogins()
    {
        /** @var Module $module */
        $module = \Yii::$app->getModule('actionlog');
        $query = $this->getLastActions();
        $query->andWhere([
            'module' => 'user',
            'controller' => 'security',
            'action' => 'login'
        ]);
        return $query;
    }


    /** {@inheritdoc} */
    public function getRecordsTilNextLogin()
    {
        $query = $this->getFollowingRecords();
        if (empty($this->nextLogin)) {
            return $query;
        }
        $query->andWhere(['<', $this->primaryKeySingle(), $this->nextLogin->primaryKey]);
        return $query;
    }

}
