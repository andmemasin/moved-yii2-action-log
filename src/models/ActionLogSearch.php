<?php

namespace andmemasin\actionlog\models;

use andmemasin\actionlog\Module;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ActionLogSearch represents the model behind the search form about `andmemasin\actionlog\models\ActionLog`.
 */
class ActionLogSearch extends ActionLog
{
    public $isSearchModel = true;

    public $username;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        /** @var Module $module */
        $module = \Yii::$app->getModule('actionlog');

        return [
            [['actionlog_id', 'user_id','model_id'], 'integer'],
            [['module', 'controller', 'action', 'status', 'message', 'time', 'ip','class'], 'safe'],
            [[$module->userNameColumn], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /** @var Module $module */
        $module = \Yii::$app->getModule('actionlog');

        $query = ActionLog::find();
        $query->joinWith([$module->userTableName]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['actionlog_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'actionlog_id' => $this->actionlog_id,
            'user_id' => $this->user_id,
            'model_id' => $this->model_id,
            'time' => $this->time,
            'class' => $this->class,
        ]);

        $query->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'controller', $this->controller])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'ip', $this->ip]);
        $query->andFilterWhere(['like', $module->userTableName . '.' . $module->userNameColumn, $this->username]);

        return $dataProvider;
    }
}
