<?php

namespace andmemasin\actionlog;

use Yii;

/**
 * Class Module
 * @package andmemasin\actionlog
 * @author Tõnis Ormisson <tonis@andmemasin.eu>
 */
class Module extends \yii\base\Module
{

    public $permissionMap = [
        'admin' => 'admin::actionlog',
    ];

    /**
     * @var string
     */
    public $userTableName = 'user';

    /**
     * @var string
     */
    public $userNameColumn = 'username';


    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'andmemasin\actionlog\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->registerTranslations();
        parent::init();
        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'andmemasin\actionlog\commands';
        }

    }

    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['actionlog/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => __DIR__ . '/messages',
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('actionlog/' . $category, $message, $params, $language);
    }
}
