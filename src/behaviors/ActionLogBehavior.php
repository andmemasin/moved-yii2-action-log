<?php

namespace andmemasin\actionlog\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use andmemasin\actionlog\models\ActionLog;

class ActionLogBehavior extends Behavior
{

    /**
     * @var string The message of current action
     */
    public $message = null;

    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterInsert($event)
    {
        ActionLog::add(ActionLog::LOG_STATUS_INFO, $this->message !== null ? $this->message : __METHOD__,null,$this->owner);
    }

    public function afterUpdate($event)
    {
        ActionLog::add(ActionLog::LOG_STATUS_INFO, $this->message !== null ? $this->message : __METHOD__,null,$this->owner);
    }

    public function afterDelete($event)
    {
        ActionLog::add(ActionLog::LOG_STATUS_INFO, $this->message !== null ? $this->message : __METHOD__,null,$this->owner);
    }

}