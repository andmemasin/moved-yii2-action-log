<?php

namespace andmemasin\actionlog\interfaces;


use yii\db\ActiveRecordInterface;

interface Loggable extends ActiveRecordInterface
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastActions();

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastLogins();

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRecords();

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFollowingRecords();

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordsTilNextLogin();

    /**
     * @return string
     */
    public function getUserColumn();




}