<?php

namespace andmemasin\actionlog\factories;


use andmemasin\actionlog\models\ActionLog;
use andmemasin\helpers\DateHelper;
use yii\console\Application;
use Yii;

class ActionLogFactory
{
    /**
     * @return ActionLog
     */
    public function makeBaseLog() {
        $model = new ActionLog();
        $model->ip = getHostByName(getHostName());
        if (!(Yii::$app instanceof Application)){
            $model->ip = $_SERVER['REMOTE_ADDR'];
        }

        if (empty(Yii::$app->requestedAction)) {
            return null;
        }

        $model->action = Yii::$app->requestedAction->id;
        $model->module = Yii::$app->controller->module->uniqueId;
        $model->controller = Yii::$app->controller->id;
        $model->time = (new DateHelper())->getDatetime6();
        return $model;

    }

}